from rest_framework.response import Response
from rest_framework import status
from client.models import Client
from rest_framework.decorators import api_view
from serializers import ClientSerializer


@api_view(['GET', 'POST'])
def client_list(request):
    """
    list all clients or create a new client
    :param request:
    :type request:
    :return:
    :rtype:
    """
    if request.method == 'GET':
        client = Client.objects.all()
        serializer = ClientSerializer(client, many=True)
        return Response(serializer.data)

    if request.method == 'POST':
        serializer = ClientSerializer(data=request.DATA)
        import ipdb;ipdb.set_trace()
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def client_detail(request, pk):
    """
    Retrieve, update or delete a snippet
    :param request:
    :type request:
    :param pk:
    :type pk:
    :return:
    :rtype:
    """

    try:
        client = Client.objects.get(pk=pk)
    except Client.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ClientSerializer(client, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.error, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        client.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)