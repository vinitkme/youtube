#! /usr/bin/env python
from rest_framework import serializers
from client.models import Client
from video.models import Video


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ('name', 'email')


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ('video_file', 'uploader')