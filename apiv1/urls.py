from django.conf.urls import patterns, url, include
from .views import client_list

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'youtube.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^clients/$', client_list, name='client_list'),
    url(r'^client/(?P<pk>[0-9]+)/$', 'client_detail'),
)
