# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0002_auto_20141025_1006'),
    ]

    operations = [
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Video File', models.FileField(upload_to=b'')),
                ('uploader', models.ForeignKey(to='client.Client')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
