from django.db import models
from client.models import Client


# Create your models here.
class Video(models.Model):
    video_file = models.FileField(name='Video File')
    uploader = models.ForeignKey(Client)