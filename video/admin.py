from django.contrib import admin
from video.models import Video

# Register your models here.
class VideoAdmin(admin.ModelAdmin):
    list_display = ['uploader']


admin.site.register(Video, VideoAdmin)