from django.contrib import admin
from client.models import Client


class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'email']
    list_filter = ['name']


admin.site.register(Client, ClientAdmin)