from django.db import models


class Client(models.Model):
    """
    Client Model
    """
    name = models.CharField(max_length=150)
    email = models.EmailField(max_length=250)

    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'


